#!/usr/bin/env python
from optparse import OptionParser 
import re
import string
import sys

def contains(theString,theQueryValue):
        return theString.find(theQueryValue) > -1

def get_number_of_aos(lines):
    '''Get the number of primitive cartesian gaussian basis functions from 
       Gamess log file'''

    p = r'NUMBER OF CARTESIAN GAUSSIAN BASIS FUNCTIONS =\s*(\d+)'
    cp= re.compile(p)

    for line in lines:
        match = cp.search(line)
        if match:
            return int(match.group(1))

def get_number_of_mos(lines):
    '''Get the number of molecular orbitals from Gammess log file.
        input:  lines    - contents of a file as a list of lines,
        output: number of AO's and number of MO's in this order.'''
#   check if the calculation is done in spherical of cartesian gaussians
#   base on the value of ISPHER keyword
    ispher_patt = r'ISPHER=\s*(\-?\d{1}).*'
    var_space_patt = r'.*VARIATION SPACE IS\s*(\d+).*'
    c_ispher = re.compile(ispher_patt)
    c_var_space = re.compile(var_space_patt)

    for line in lines:
        match = c_ispher.search(line)
        if match:
            ispher = int(match.group(1)) 

    n_ao = get_number_of_aos(lines)
    if ispher == -1:
        n_mo = n_ao
        return n_ao, n_mo
    elif ispher == 1:
        for line in lines:
            match = c_var_space.search(line)
            if match:
                n_mo = int(match.group(1))
                return n_ao, n_mo
    else:
        sys.exit("ispher not found")  

def get_lz_values(lines, orbitals):
    '''Get values of Lz operator for every orbital and translate them into labels of orbotals
        input:  lines    - contents of a file as a list of lines,
        output: orbitals - list of dict containing MO indexes and labels (sigma, pi ...)'''
    
    lz_patt = r'\s*MO\s*(\d*).*HAS LZ\(WEIGHT\)=\s*(-?\d*\.\d+)'
    c_lz = re.compile(lz_patt)

    labels = {0:"sigma",1:"pi",2:"delta",3:"phi",4:"gamma"}

    for line in lines:
        match = c_lz.search(line)
        if match:
            i_mo = int(match.group(1))
            lz   = abs(int(float(match.group(2))))
            orbitals.append({"index":i_mo,"label":labels[lz]})

def slice_list_between(start_string, end_string, list_to_slice):

    c_start = re.compile(start_string)
    c_end   = re.compile(end_string)    

    for i, line in enumerate(list_to_slice):
        match1 = c_start.search(line)
        match2 = c_end.search(line)
        if match1:
            i_start = i
        if match2:
            i_end = i
    return i_start, i_end

def get_ao_info(lines, ao_info):
    '''Retrieve the information about atomic basis set'''
    
    pat = r'\s*(\d+)\s{2}(.{1,2})\s*(\d+)\s*([A-Z]{1,}).*'
    c_pat = re.compile(pat)

    for line in lines:
        match = c_pat.search(line)
        if match:
            index = int(match.group(1))
            atom  = match.group(2)
            center = int(match.group(3))
            function = match.group(4)
            ao_info.append({"index":index,"atom":atom,"center":center,"g_type":function})

def get_symmetry_labels(lines, orbitals):
    '''Retrieve orbital symmetry labels from orbital print format'''

    temp = []
    pat = r'\s{15,}[A-Z]+'
    c_pat = re.compile(pat)

    for line in lines:
        match = c_pat.search(line)
        if match:
            temp.extend(string.split(line))
    for i, item in enumerate(orbitals):
        orbitals[i]["irrep"] = temp[i]

def get_orbital_eigenvalues(lines, orbitals):
    '''Get orbital eigenvalues form the log file formatted output. 
       In case of HF MO's those will be orbital energies, in case of 
       NO's - occupation numbers.'''

    temp = []
    pat = r'\s{15,}-?\d*\.\d+'
    c_pat = re.compile(pat)

    for line in lines:
        match = c_pat.search(line)
        if match:
            temp.extend(string.split(line))

    for i, item in enumerate(orbitals):
        try:
            orbitals[i]["eigenval"] = float(temp[i])
        except (ValueError, IndexError):
            orbitals[i]["eigenval"] = 0.0

def get_orbitals(datfile, n_ao, n_mo, orb_type, orbitals):
    '''Get molecular orbital coefficients over atomic basis set from the *.dat file'''
    
    temp = []; contents = []
    vec = " $VEC"; end = " $END"
    dat = open(datfile, 'r')
    line = dat.readline()
    while not contains(line, orb_type):
        line = dat.readline()
    while not contains(line, vec):
        line = dat.readline()
    while not contains(line, end):
        line = dat.readline()
        contents.append(line)
        
    if n_ao % 5 == 0:
        n_lines = n_ao/5
    else:
        n_lines = n_ao/5+1
    ij = -1 
    for i in range(0, n_mo):
        coeffs = []
        for j in range(0, n_lines):  
            temp = []
            ij = ij + 1 
            for k in range(0,5):
                temp.append(contents[ij][5+15*k:5+15*k+15])
            coeffs.extend(float(item) for item in temp if len(item) == 15 )      
        orbitals[i]["coeffs"] = coeffs
    dat.close()

if __name__ == "__main__":
    
    parser = OptionParser()
    parser.add_option("-l", "--logfile",action="store",type="string",dest="logfile",default="",help="log file name")
    parser.add_option("-d", "--datfile",action="store",type="string",dest="datfile",default="",help="dat file name")
    parser.add_option("-a", "--ao_coeff",action="store_true",dest="do_ao_coeffs",default=False, 
                      help="print MO epansion coefficients over AO's")
    (opts, args) = parser.parse_args()
    
    hf = "--- CLOSED SHELL ORBITALS ---"
    no = "GUGA-CI NO-S"

    log = open(opts.logfile, 'r')
    lines = log.readlines()
    ao, mo = get_number_of_mos(lines)
    print("{0:25s} = {1: >5d}\n{2:25s} = {3: >5d}".format("number of cartesian AO's",ao,"number of HF MO's",mo))
    orbitals = []
    ao_info  = []
    get_lz_values(lines, orbitals)
    for i in range(0, mo):
        orbitals.append({"index":i+1})

    #i_sta, i_end = slice_list_between(r'\s*EIGENVECTORS\s*',r'.*END OF RHF CALCULATION.*',lines)
    i_sta, i_end = slice_list_between(r'\s*NATURAL ORBITALS IN ATOMIC\s*',r'.*END OF DENSITY MATRIX CALCULATION.*',lines)
    get_ao_info(lines[i_sta+6:i_sta+6+ao], ao_info)
    get_symmetry_labels(lines[i_sta:i_end],orbitals)
    get_orbital_eigenvalues(lines[i_sta:i_end],orbitals)
    log.close()
    if opts.do_ao_coeffs:
        get_orbitals(opts.datfile, ao, mo, no, orbitals)
        if "label" in orbitals[0].keys():
            for i, item in enumerate(orbitals):
                print("\nMO {0: >4d} {1: <10s} {2:<5s}  EV = {3:>10.4f}".format(orbitals[i]["index"],
                    orbitals[i]["label"],orbitals[i]["irrep"],orbitals[i]["eigenval"]))
                for j in range(0, len(orbitals[i]["coeffs"])):
                    if abs(orbitals[i]["coeffs"][j]) >= 0.1:
                        print("{0:>4d}{1:>3s}{2:>3d}{3:>5s}{4:15.6f}".format( 
                            ao_info[j]["index"],
                            ao_info[j]["atom"],
                            ao_info[j]["center"],
                            ao_info[j]["g_type"],
                            orbitals[i]["coeffs"][j]))
        else:
            for i, item in enumerate(orbitals):
                print("\nMO {0: >4d} {1:<5s}  EV = {2:>10.4f}".format(orbitals[i]["index"],
                    orbitals[i]["irrep"],orbitals[i]["eigenval"]))
                for j in range(0, len(orbitals[i]["coeffs"])):
                    if abs(orbitals[i]["coeffs"][j]) >= 0.1:
                        print("{0:>4d}{1:>3s}{2:>3d}{3:>5s}{4:15.6f}".format( 
                            ao_info[j]["index"],
                            ao_info[j]["atom"],
                            ao_info[j]["center"],
                            ao_info[j]["g_type"],
                            orbitals[i]["coeffs"][j]))
    else:
        if "label" in orbitals[0].keys():
            for i, item in enumerate(orbitals):
                print("MO {0: >4d} {1: <10s} {2:<5s}  EV = {3:>10.4f}".format(orbitals[i]["index"],
                    orbitals[i]["label"],orbitals[i]["irrep"],orbitals[i]["eigenval"]))
        else:
            for i, item in enumerate(orbitals):
                print("MO {0: >4d} {1:<5s}  EV = {2:>10.4f}".format(int(orbitals[i]["index"]),
                    str(orbitals[i]["irrep"]),float(orbitals[i]["eigenval"])))
